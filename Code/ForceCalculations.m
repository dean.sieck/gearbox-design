
clc, clear all, close all

%% ~~~~~~~~~~~~~~~    Design Constraints   ~~~~~~~~~~~~~~~~~~~~~~~~
SafetyFactor=3; 
MotorPower=20;      % Hp
MotorSpeed = 9600;  % rpm
OutputSpeed = 80;   % rpm
DesignLife = 15000; % hours
Reliability = .99;  % manufacturer 2
PA = 20*pi/180;     % radians
gap=0.25;           % in
stages=4;
mm=1/25.4;          %mm/in

%struct to organize data needed for drawings in
GearBox=struct;

%% ~~~~~~~~~~~~~~~    Force Calculations   ~~~~~~~~~~~~~~~~~~~~~~~~
%tested on work shown in apendix

% d1=2; D=2.5;
% while (D+gap)>d1
% d1=d1+.01;   

% m=(MotorSpeed/OutputSpeed)^(1/4);
% d1=2.4;        %small gear diameter
% d2=d1*m;   %large Gear diameter

d1=3.75;
d2=12.5;
m=d2/d1;
GearBox.SmallGearDiameter=d1;
GearBox.LargeGearDiameter=d2;

r1=d1/2;
r2=d2/2;

% ~~~  ~~~  ~~~  ~~~ gear #2 => Stage 0 ~~~  ~~~  ~~~  ~~~
disp ('gear 2')
Ta2=MotorPower*63025/MotorSpeed;
F32t=Ta2/r1;
F32n=F32t*tan(PA);
F32=sqrt(F32t^2+F32n^2);
Fa2n=F32n;
Fa2t=F32t;
Fa2=F32;
D=ShaftDiameterCalculations(0, Fa2);
%Bearings
R=Fa2*(6.4-.7)/6.4;
Speed=MotorSpeed/(m^0);
[Bd, Od, Bw, Br]=BearingDesign(R,Speed);
GearBox.a.ShaftDiameter=D;
GearBox.a.BoreDiameter=Bd;
GearBox.a.OuterBearingDiameter=Od;
GearBox.a.BearingWidth=Bw;
GearBox.a.BearingFilletRadius=Br;
%Gear
[FS,GT1]=Gears(d1,F32t,Speed);
GT1=1.1;
GearBox.d.SmallGearThickness=GT1;
 
% ~~~  ~~~  ~~~  ~~~ gear #3 => Stage 1 ~~~  ~~~  ~~~  ~~~
disp ('gear 3')
F43t=F32t*r2/r1;
F43n=F43t*tan(PA);
F43=sqrt(F43t^2+F43n^2);
Fb3n=F43n-F32n;
Fb3t=F43t+F32t;
Fb3=sqrt(Fb3t^2+Fb3n^2);
D=ShaftDiameterCalculations(1, Fb3);
%Bearings
R=Fa2*(6.4-1.15)/6.4;
Speed=MotorSpeed/(m^1);
[Bd, Od, Bw, Br]=BearingDesign(R,Speed);
GearBox.b.ShaftDiameter=D;
GearBox.b.BoreDiameter=Bd;
GearBox.b.OuterBearingDiameter=Od;
GearBox.b.BearingWidth=Bw;
GearBox.b.BearingFilletRadius=Br;
%Gears
[FS,GT2]=Gears(d1,F43t,Speed);
GT2=1.1;
[FS]=Gears(d2,F32t,Speed);
GearBox.b.SmallGearThickness=GT2;
GearBox.b.LargeGearThickness=GT1;

% ~~~  ~~~  ~~~  ~~~ gear #4 => Stage 2 ~~~  ~~~  ~~~  ~~~
disp ('gear 4')
F54t=(F43t*r2)/r1;
F54n=F54t*tan(PA);
F54=sqrt(F54t^2+F54n^2);
Fc4n=F54n-F43n;
Fc4t=F54t+F43t;
Fc4=sqrt(Fc4t^2+Fc4n^2);
D=ShaftDiameterCalculations(2, Fc4);
%Bearings
R=Fa2*(6.4-2.15)/6.4;
Speed=MotorSpeed/(m^2);
[Bd, Od, Bw, Br]=BearingDesign(R,Speed);
GearBox.c.ShaftDiameter=D;
GearBox.c.BoreDiameter=Bd;
GearBox.c.OuterBearingDiameter=Od;
GearBox.c.BearingWidth=Bw;
GearBox.c.BearingFilletRadius=Br;
%gears
[FS,GT3]=Gears(d1,F54t,Speed);
[FS]=Gears(d2,F43t,Speed);
GearBox.c.SmallGearThickness=GT3;
GearBox.c.LargeGearThickness=GT2;

% ~~~  ~~~  ~~~  ~~~ gear #5 => Stage 3 ~~~  ~~~  ~~~  ~~~
disp('gear 5')
F65t=(F54t*r2)/r1;
F65n=F65t*tan(PA);
F65=sqrt(F65t^2+F65n^2);
Fd5n=F65n-F54n;
Fd5t=F65t+F54t;
Fd5=sqrt(Fd5t^2+Fd5n^2);
D=ShaftDiameterCalculations(3, Fd5);
%Bearings
R=Fa2*4.3/6.4;
Speed=MotorSpeed/(m^3);
[Bd, Od, Bw, Br]=BearingDesign(R,Speed);
GearBox.d.ShaftDiameter=D;
GearBox.d.BoreDiameter=Bd;
GearBox.d.OuterBearingDiameter=Od;
GearBox.d.BearingWidth=Bw;
GearBox.d.BearingFilletRadius=Br;
%gears
[d1,d2,f]=LimitingGear(d1,F65t,Speed);
GT4=f;
[FS]=Gears(d2,F54t,Speed);
GearBox.d.SmallGearThickness=GT4;
GearBox.d.LargeGearThickness=GT3;
 
% ~~~  ~~~  ~~~  ~~~ gear #6 => Stage 4 ~~~  ~~~  ~~~  ~~~ 
disp ('gear 6')
Te6=F65t*r2;
Fe6n=F65n;
Fe6t=F65t;
Fe6=F65;
Dmax=OutputShaftMaxDiameter(Fe6,Te6/2);
%Bearing loads
R2=Fe6*4.85/6.4;  R1=Fe6-R2;
%large bearing
[Bd, Od, Bw, Br]=BearingDesign(R2,MotorSpeed/(m^4));
%Bearing Too large for shaft - use cylindrical bearing
Bd=60*mm; Od=110*mm; Bw=22*mm; Br=1*mm; %Br not given?
GearBox.e.MaxShaftDiameter=Dmax;
GearBox.e.B1.BoreDiameter=Bd;
GearBox.e.B1.OuterBearingDiameter=Od;
GearBox.e.B1.BearingWidth=Bw;
GearBox.e.B1.BearingFilletRadius=Br;
%Small Bearing
Speed=MotorSpeed/(m^4);
[Bd, Od, Bw, Br]=BearingDesign(R1,Speed);
%bearing too small for output shaft max torque
Bd=50*mm; Od=90*mm; Bw=20*mm; Br=1*mm; 
%shaft design
[FS,Dmid]=FancyOutputShaft(Fe6, Te6/2, Dmax);
GearBox.e.MiddleShaftDiameter=Dmid;
Bd=50*mm; Od=90*mm; Bw=20*mm; Br=1*mm; %Br not given? 
GearBox.e.B2.BoreDiameter=Bd;
GearBox.e.B2.OuterBearingDiameter=Od;
GearBox.e.B2.BearingWidth=Bw;
GearBox.e.B2.BearingFilletRadius=Br;
%gears
Gears(d2,F65t,Speed);
GearBox.e.GearThickness=GT4;

%% ~~~~~~~~~~~~~~~    Box Size   ~~~~~~~~~~~~~~~~~~~~~~~~
length=d2+2*gap;
height=d1+(stages-1)*(d2/2+d1/2)+d2+2*gap;
width=GT1+GT2+GT3+GT4+2*gap;
volume=length*width*height/(12^3);

%Wall thickness
Fw=.5*Te6/(d2+.5);

R2y=Fe6t*(1.55/6.4)-Fw;
R1y=Fe6t-2*Fw-R2y;
R2x=Fe6n*(1.55/6.4);
R1x=Fe6n-R2x;
R1=sqrt(R1x^2+R1y^2);
R2=sqrt(R2x^2+R2y^2);
thickness=WallThickness(R1,length,Od,Bw);

GearBox.length=length;
GearBox.width=width;
GearBox.height=height;
GearBox.thickness=thickness;

