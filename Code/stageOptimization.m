%tested on gears problem set
function [dp,dg,Np,Ng]=stageOptimization(Np,P)
%% ~~~~~~~~~~~~~~~    Design Constraints   ~~~~~~~~~~~~~~~~~~~~~~~~
SafetyFactor=3; 
MotorPower=15e3;    % W
MotorSpeed = 9600;  % rpm
OutputSpeed = 80;   % rpm
DesignLife = 15000; % hours
Reliability = .99;  % manufacturer 2
PressureAngle = 20*pi/180;  % radians



%% ~~~~~~~~~~~~~~~    Stage Optimization   ~~~~~~~~~~~~~~~~~~~~~~~~
%step 1
stages=4;
e=MotorSpeed/OutputSpeed;
m=e^(1/stages);
%step 2
k=1;
pa=PressureAngle;
%Np=round(2*k/((1+2*m)*sin(pa)^2)*(m+sqrt(m^2+(1+2*m)*sin(pa)^2)));
%step 3
Ng=round(Np*m);
%step 4
GearRatio=(Ng/Np)^stages;
m=GearRatio^(1/stages);
%step 5
%P=40;                      
dp=(Np/P);
dg=(Ng/P);
gap=0.25;
GearThickness=1;
length=dg+2*gap;
height=dp+(stages-1)*(dg/2+dp/2)+dg+2*gap;
width=GearThickness*stages+2*gap;
volume=length*width*height/(12^3);
OutputSpeed=MotorSpeed/GearRatio;

% fprintf(' Stages: %.0f \n Gear Ratio: %.2f \n', stages,GearRatio)
% fprintf(' Small Diameter: %.2f in \n Large Diameter: %.2f in \n', dp,dg)
% fprintf(' Output Speed: %.2f rpm\n Volume: %.2f ft^3\n',OutputSpeed,volume)
end