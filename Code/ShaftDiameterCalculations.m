%Tested on HW7 & midterm
function [d]=ShaftDiameterCalculations(stage, R) 

%material properties
Sut=125e3;      %ultimate strength
Sy=102e3;       %yeild strength


%Critical Locations
T = [ 0     0      0      0 ]; %torque at critical locations
Kts=[ 1     1      1      1 ];

switch stage % parts that could/do change with each rod
    case 0
        X = [ 0    0.7     1.4     6.4 ];
        T = [ 0     0     131.3   131.3]; 
        Kt= [ 1     1      1.7      1  ];   %
        Kts=[ 1     1      1.4      1  ];   %
    case 1
        X = [ 0    1.15    2.3     6.4 ]; 
        Kt= [ 1     1      1.8      1 ];    %
    case 2
        X = [ 0    2.15    3.4     6.4 ]; 
        Kt= [ 1     1      2.2      1 ];    %
    case 3
        X = [ 0     1.8    4.3     6.4 ]; 
        Kt= [ 1     2.6     1       1 ];    %
end


  
%point forces at critical locations
if stage ~=3
    R2=R*X(2)/6.4;  R1=R-R2;
    F = [R1   -R      0     R2]; 
    q = [ 0    0     0.9     0]; 
    gear=X(2);
else
    R2=R*X(3)/6.4;  R1=R-R2;
    F = [R1    0     -R     R2];
    q = [ 0   0.9     0     0]; 
    gear=X(3);
end
    
    
for d=.8268:.01:5
D = [d     d      d      d];%diameter at critical locations
Ka=14.4*(Sut*10^-3)^-0.718;
if d<=2
    Kb=0.879.*D.^(-0.107);    
else
    Kb=0.91.*D.^(-0.157); 
end
    
Kc=1;


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

% find Minimum diameter for Fatigue
Kf=1+q.*(Kt-1);
Kfs=1+q.*(Kts-1);
M=zeros(1,length(X));
V=zeros(1,length(X));
for i=1:length(X)
    for j=1:i-1
        M(i)=max(0,M(i)+F(j)*(X(i)-X(j)));
    end
end
I=pi.*D.^4./64;
stress=Kf.*M.*(D./2)./I;
i=find(stress==max(stress));
K=Ka.*Kb.*Kc;

%find Se'
if Sut<=200e3   
    Se=0.5*Sut;
elseif Sut<=1400e6
    Se=700e6;
else
    Se=100e3;
end
%find actual Se
Se=K*Se;

%Factor of Safety at each point
Om=(0:1:Sy);
for p=1:length(Se)
%failure line
Oa=min(Se(p)-Se(p)/Sut*Om,Sy-Om);

%Current load
stressTorq=16*T(p)/(pi*D(p)^3)*Kfs(p); %mean only
stressBend=32*M(p)/(pi*D(p)^3)*Kf(p); %alternating only
alt=sqrt(stressBend^2+0);
mean=sqrt(0+3*stressTorq^2);
maxStress(p)=alt+mean;

%distance to failure
a=sqrt(mean^2+alt^2);
dOa=Oa-alt;
dOm=Om-mean;
b=min(sqrt(dOa.^2+dOm.^2));
FS(p)=(a+b)/a;
end

if min(FS)>=3
    break
end

end

if true
    name=sprintf ('Stage #%0.0f Shaft Forces',stage);
figure('Name',name);
subplot (3,1,1)
x=[0,gear,gear,6.4];
V=[F(1),F(1),-F(4),-F(4)];
plot (x,0*x,':k')
hold on
plot (x,V)
title('Shear')
xlabel('X (in)')
ylabel('Shear (lb)')
xlim([0,6.4])

subplot (3,1,2)
x=[0,X(3),X(3),6.4];
T=[T(1),T(1),T(length(T)),T(length(T))];
plot (x,0*x,':k')
hold on
plot (x,T)
title('Torque')
xlabel('X (in)')
ylabel('Torque (in*lb)')
xlim([0,6.4])

subplot (3,1,3)
plot (x,0*x,':k')
hold on
plot (X,M)
title('Moment')
xlabel('X (in)')
ylabel('Moment (in*lb)')
xlim([0,6.4])
end

if false
min (FS)
%fprintf('Pin: r/d=%0.2f   D/d=%0.2f\n\n',(1/16)/(d-1/8),d/(d-1/8))
end
end

