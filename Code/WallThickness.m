%tested on midterm
function [t]=WallThickness(P,length,Od,Bw)

%Material Constants (psi)
Sy=40000; 
SU=45000;
E=10000e3;
p=0.0975; % density: lb/in�

%wall constants
P=P*3;    %Load (including FS=3)
C=1/4; %fixed free
L=length/2;
w=Od;
tmax=Bw;

%find minimum thickness
t=.001;
Pcr=0;
while Pcr<P
    t=t+.0001;
    A=t*w;

    lk1=sqrt(2*pi*C*E/Sy);
    lk=L/sqrt(t^2/12);

    if lk>lk1
        Pcr=A*(C*pi^2*E/(lk^2));
    else
        Pcr=A*(Sy-(Sy/(2*pi)*lk)^2*1/(C*E));
    end
end
t;
%find intersect between left and right sides of equation
t=(t:.0001:tmax/2);
%left side
A=t*w;
left=P./A;
%right side
c=t/2;
e=tmax/2-c;
right=Sy./(1+e.*c./(t.^2/12).*sec(.5*lk.*sqrt(P./(A.*E))));
 
% plot (t,left)
% hold on
% plot (t,right)
% legend('left','right')

dif=abs(left-right);
t=t(find(dif==min(dif)));

end
