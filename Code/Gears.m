%tested on Gears Problem Set
function [FS,f] = Gears(d,Wt,Speed)

%table 14-2
Nt=[ 12    13    14    15    16    17    18    19    20    21    22    24    26    28    30    34    38    43    50    60    75   100   150   300   400];
Yt=[  0.2450    0.2610    0.2770    0.2900    0.2960    0.3030    0.3090    0.3140 0.3220    0.3280    0.3310    0.3370    0.3460    0.3530    0.3590    0.3710  0.3840    0.3970    0.4090    0.4220    0.4350    0.4470    0.4600    0.4720   0.4800];

for f=1.1:.1:3           %face width
PA=20;          %Pressure Angle

P=4;             %diametral Pitch

Sut=95.7;
Sy=81.2;

N=(P*d);              %Number of Teeth  
Y=Yt(max(find(Nt<=N)));
v=pi*d*Speed/12;    %velocity
Kv=(1200+v)/1200;

stress=Kv*Wt*P/(f*Y)*(10^-3);

%yeild stress
FSy=Sy/stress;

%gear thickness
x=3*Y/(2*P);
l=1/P+1.25/P;                         
t=sqrt(4*l*x);

%fatigue stress
Ka=2.7*Sut^-0.265;
Kb=.879*(.808*sqrt(l*f))^(-.107);
Se=.5*Sut*Ka*Kb;
alt=stress/2;
FS=(1/(alt/Se+alt/Sut));

FS=min(FS,FSy);

if FS>3
    break
end
end


if false
    f
    d
    stress
    FSy
    Se
    FS
end

end

