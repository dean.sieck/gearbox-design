%tested on HW7 & midterm
function [FS,d]=FancyOutputShaft(R, Tg, Dmax)

%material properties
Sut=125e3;      %ultimate strength
Sy=102e3;       %yeild strength

%Bearing fillet Radius
Br=1/25.4;  %1mm
Bd2=60/25.4;
Bd1=50/25.4;

%Critical Locations
X = [ 0     2.8    3.3     4.85     6.4 ];
T = [ Tg    Tg     Tg     -Tg       -Tg ]; %torque at critical locations
Kts=[ 1    1.5     1.3     1        1.7 ];     %
Kt= [ 1    2.0     1.9     1        2.5 ];     %
R2=R*X(4)/6.4;  R1=R-R2;
F = [ R1    0      0       -R        R2 ];
q = [ 0    0.85    0.9      0       0.85 ];


for d=Bd1+Br:.01:Dmax
    D = [Bd1     d    Dmax      Dmax     Bd2];%diameter at critical locations
    Ka=14.4*(Sut*10^-3)^-0.718;
    if d<=2
        Kb=0.879.*D.^(-0.107);
    else
        Kb=0.91.*D.^(-0.157);
    end
    
    Kc=1;
    
    
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    
    % find Minimum diameter for Fatigue
    Kf=1+q.*(Kt-1);
    Kfs=1+q.*(Kts-1);
    M=zeros(1,length(X));
    V=zeros(1,length(X));
    for i=1:length(X)
        for j=1:i-1
            M(i)=M(i)+F(j)*(X(i)-X(j));
        end
    end
    I=pi.*D.^4./64;
    stress=Kf.*M.*(D./2)./I;
    i=find(stress==max(stress));
    K=Ka.*Kb.*Kc;
    
    %find Se'
    if Sut<=200e3
        Se=0.5*Sut;
    elseif Sut<=1400e6
        Se=700e6;
    else
        Se=100e3;
    end
    %find actual Se
    Se=K*Se;
    
    %Factor of Safety at each point
    Om=(0:1:Sy);
    for p=1:length(Se)
        %failure line
        Oa=min(Se(p)-Se(p)/Sut*Om,Sy-Om);
        
        %Current load
        stressTorq=16*T(p)/(pi*D(p)^3)*Kfs(p); %mean only
        stressBend=32*M(p)/(pi*D(p)^3)*Kf(p); %alternating only
        alt=sqrt(stressBend^2+0);
        mean=sqrt(0+3*stressTorq^2);
        maxStress(p)=alt+mean;
        
        %distance to failure
        a=sqrt(mean^2+alt^2);
        dOa=Oa-alt;
        dOm=Om-mean;
        b=min(sqrt(dOa.^2+dOm.^2));
        FS(p)=(a+b)/a;
    end
    
    if min(FS)>3
        break
    end
    
end


if true
    %constants that will be needed updating this program
    D
    X
    FS
    fprintf('shelf: r/d=%0.2f   D/d=%0.2f\n\n',Br/d,Dmax/d)
end
end

