%tested on HW7 & midterm
function [d]=OutputShaftMaxDiameter(R, Tg) 

%material properties
Sut=125e3;      %ultimate strength
Sy=102e3;       %yeild strength

%Critical Locations
T = [ Tg    Tg    -Tg   -Tg ]; %torque at critical locations
X = [ 0     3.3    4.85    6.4 ];
Kts=[ 1     1.6      1     1 ];
Kt= [ 1     2.4      1     1 ];    %
R2=R*X(3)/6.4;  R1=R-R2;
F = [R1    0     -R     R2];
q = [ 0   0.9     0     0];
    
    
for d=.5:.01:5
D = [d     d      d      d];%diameter at critical locations
Ka=14.4*(Sut*10^-3)^-0.718;
if d<=2
    Kb=0.879.*D.^(-0.107);    
else
    Kb=0.91.*D.^(-0.157); 
end
    
Kc=1;


% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

% find Minimum diameter for Fatigue
Kf=1+q.*(Kt-1);
Kfs=1+q.*(Kts-1);
M=zeros(1,length(X));
V=zeros(1,length(X));
for i=1:length(X)
    for j=1:i-1
        M(i)=M(i)+F(j)*(X(i)-X(j));
    end
end
I=pi.*D.^4./64;
stress=Kf.*M.*(D./2)./I;
i=find(stress==max(stress));
K=Ka.*Kb.*Kc;

%find Se'
if Sut<=200e3   
    Se=0.5*Sut;
elseif Sut<=1400e6
    Se=700e6;
else
    Se=100e3;
end
%find actual Se
Se=K*Se;

%Factor of Safety at each point
Om=(0:1:Sy);
for p=1:length(Se)
%failure line
Oa=min(Se(p)-Se(p)/Sut*Om,Sy-Om);

%Current load
stressTorq=16*T(p)/(pi*D(p)^3)*Kfs(p); %mean only
stressBend=32*M(p)/(pi*D(p)^3)*Kf(p); %alternating only
alt=sqrt(stressBend^2+0);
mean=sqrt(0+3*stressTorq^2);
maxStress(p)=alt+mean;

%distance to failure
a=sqrt(mean^2+alt^2);
dOa=Oa-alt;
dOm=Om-mean;
b=min(sqrt(dOa.^2+dOm.^2));
FS(p)=(a+b)/a;
end

if min(FS)>=3
    break
end

end


if true
figure('Name','Output Shaft Forces');
subplot (3,1,1)
x=[0,X(3),X(3),6.4];
V=[F(1),F(1),-F(4),-F(4)];
plot (x,0*x,':k')
hold on
plot (x,V)
title('Shear')
xlabel('X (in)')
ylabel('Shear (lb)')
xlim([0,6.4])

subplot (3,1,2)
x=[0,X(2),X(2),6.4];
T=[T(1),T(1),T(length(T)),T(length(T))];
plot (x,0*x,':k')
hold on
plot (x,T)
title('Torque')
xlabel('X (in)')
ylabel('Torque (in*lb)')
xlim([0,6.4])

subplot (3,1,3)
plot (x,0*x,':k')
hold on
plot (X,M)
title('Moment')
xlabel('X (in)')
ylabel('Moment (in*lb)')
xlim([0,6.4])
end

if false
%constants that will be needed updating this program
FS
fprintf('Pin: r/d=%0.2f   D/d=%0.2f\n\n',(1/16)/(d-1/8),d/(d-1/8))
end
end

