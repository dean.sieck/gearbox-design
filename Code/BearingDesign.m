%tested on HW11
function [Bd, Od, Bw, Br]=BearingDesign(Load,Speed);

%Convert Load to KN
Load = Load*0.00444822162;

         % Bd Od Bw Br C10
Bearings=csvread('bearings');
%convert dimensions to in
mm=1/25.4;
Bearings(:,1:4)=Bearings(:,1:4)*mm;

DesignLife=15000;
ApplicationFactor=1;

Ld=DesignLife*Speed*60;
Lr=1e6;
Xd=Ld/Lr;
Fd=Load*ApplicationFactor;
C10=Fd*Xd^(1/3);

R=0;
while R<.99
    b=min(find(Bearings(:,5)>C10));
    if ~isempty(b) 
        Bd=Bearings(b,1);
        Od=Bearings(b,2);
        Bw=Bearings(b,3);
        Br=Bearings(b,4);
        C10=Bearings(b,5);
    else
          disp ('need cylindrical roller bearing')
          fprintf ('R=%.2f C10 = %.1f\n',R*100,C10)
          Bd=input('Bore Diameter: ');
          Od=input('Outer Diameter: ');
          Bw=input('Width: ');
          Br=2;
          C10=input('C10: ');
      
    end

    Xo=0.02; O=4.459; b=1.483;
    R=exp(-((Xd*(Fd/C10)^3-Xo)/(O-Xo))^b);
end

end

